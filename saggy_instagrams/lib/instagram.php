<?php
/***
 * Based on http://instagram.com/developer/endpoints/
 * This is a partial implementation of the Instagram API. Several endpoints were not included
 * because we don't use them at Chirpify.
 * Many endpoints have additional optional parameters, which we aren't using.
 * A sample implentation of the optional parameters is included in the  get_user_recent_media() method
 * 
 * IMPORTANT: Most of these functions require the access token.
 * to get an access token, you need to get an authorized user for the client. 
 * when you register your app at http://instagram.com/developer/register/
 * you receive a CLIENT_ID and CLIENT_SECRET
 * 
 * if you don't need to be authorized, you only need a client id. 
 * 
 */

// Require the http request lib from the parent module
require_once(drupal_get_path('module', 'saggy') . '/lib/http_request.php');

class Instagram
{

	var $base_url = 'https://api.instagram.com/v1/';
	var $client_id;
	var $client_secret;
	var $redirect_uri;
	var $access_token;

	function __construct($client_id, $client_secret, $redirect_uri, $access_token = null) {
		$this->request = new HTTPRequest();
		$this->client_id = $client_id;
		$this->client_secret = $client_secret;
		$this->redirect_uri = $redirect_uri;
		$this->access_token = $access_token;
	}

	
	/**
	* Returns information on a particular media_id
	* 
	* ### Returns
	*
	*   * access token var
	*/

	public function get_access_token($auth_code) {

		$endpoint = "https://api.instagram.com/oauth/access_token";
		$payload = array(
			'client_id' => $this->client_id,
			'client_secret' => $this->client_secret,
			'grant_type' => 'authorization_code',
			'redirect_uri' => $this->redirect_uri,
			'code' => $auth_code
		);
		
		$result = $this->request->post($endpoint, $payload);
		
		return $result;
	}

	/**
	* Returns information on a particular media_id
	* 
	* ### Requires
	*  * `access_token` (string) Instagram users' oauth OR `client_id`
	*	* `media_id` (string)   ID of the instagram media
	* 
	* ### Returns
	*
	*   * JSON object pertaining to a piece of media
	*/
	/*
	public function get_media($media_id, $access_token) {
		$endpoint = $this->base_url."media/$media_id";

		$payload = array(
		'access_token' => $access_token		
		);

		$result = $this->http_request->get($endpoint, $payload);

		return $result;
	}
	*/

	/**
	* Returns a user's recent media feed
	* 
	* ### Requires
	*
	* 	 * `access_token` (string) Instagram user's oauth
	*	 * `user_id` (string) Instagram user's ID
	* 
	* ### OPTIONAL
	* Several Instagram API methods offer optional data, such as count, lat/long, min/max timestamps. They are implemented as an example here.
	* 
	*  count	Count of media to return.
	*	min_id (UTC TIMESTAMP)	Return media later than this min_id.
	*	max_id (UTC TIMESTAMP)	Return media earlier than this max_id.s
	* 
	* ### Returns
	*
	*   * JSON object with user data
	*/
	public function get_tag_recent_media($tag, $options=NULL) {

		$payload = array(
			'access_token' => $this->access_token
		);

		if(is_array($options)) {
			foreach ($options as $key => $value) {
				$payload[$key] = $value;
			}
		}
	
		$endpoint = $this->base_url."/tags/" . $tag . "/media/recent";
		$result = $this->request->get($endpoint,$payload);
	
		return $result;
	}
}
